import express from 'express'
import config from 'config'

import router from './router/router'
import ewelinkConnect from './ewelink-connect'

const port = config.get('app.port')
const app = express()

ewelinkConnect()

app.use(express.json())
app.use(router)
app.listen(port, () => { console.log(`Server Startd on Port ${port}`) })
