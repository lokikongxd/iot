import { Router } from 'express'

import doorController from './ewelink/doorController'

const router = Router()

router.post('/door', doorController)

export default router
