import config from 'config'

import { connection } from '../../ewelink-connect'

const deviceId = config.get('ewelink.device_id')
const unlockTime = config.get('ewelink.time')
let lockState = 'on'
let timeoutState

const doorController = async (req, res) => {
  try {
    const { recognition } = req.body
    if (recognition === true) {
      lockState = 'off'
      await connection.setDevicePowerState(deviceId, lockState) // unlock
      res.status(200).json({ status: 200, message: 'เปิดประตู' })
      if (lockState === 'off') {
        clearTimeout(timeoutState) // clear timeout when the scan is repeated within 5 seconds
        console.log('clear1')
      }
      timeoutState = setTimeout(async () => {
        console.log('clear2')
        lockState = 'on'
        await connection.setDevicePowerState(deviceId, lockState) // lock door
      }, unlockTime) // wait time
    } else {
      res.status(400).json({ status: 400, message: 'ไม่พบการยืนยันใบหน้า' })
    }
  } catch (error) {
    console.log(error)
  }
}

export default doorController
