import Ewelink from 'ewelink-api'
import config from 'config'

export const connection = new Ewelink({
  email: config.get('ewelink.email'),
  password: config.get('ewelink.pass'),
  region: config.get('ewelink.region'),
})

const ewelinkConnect = async () => {
  const device = await connection.getDevice(config.get('ewelink.device_id')) // get device id
  console.log(`ewelink device id: ${device._id}`)
}

export default ewelinkConnect
